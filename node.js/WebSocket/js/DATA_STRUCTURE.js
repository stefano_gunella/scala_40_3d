// import {CARD_CONST}  from './CONFIG.js'; KO
const CONFIG = require('./CONFIG'); // OK

// const functions = require("firebase-functions");

// ==============================================
// aggiungo la funzione di mescolamento all'Array
// ==============================================
Array.prototype.shuffle = function(iterazioni) {
	for (let n = 0; n < iterazioni; n++) {
		console.log("*** SHUFFLE #",n);
   		for (let i = this.length - 1; i > 0; i--) {
    		const j = Math.floor(Math.random() * (this.length - 1));
    	 	[this[i], this[j]] = [this[j], this[i]];
			if(CONFIG.DEBUG){
				console.log("scambio la carta["+i + "] con la carta[" + j + "]");
			}
    	}
	}
};

// rimuove una carta all'interno dell'array e restituisce la posizione
Array.prototype.remove = function(card) {
	var result = -1;
	var item;
	for (let i = 0; i < this.length; i++) {
 	   item = this[i];
		if (item.equals(card)){
			this.splice(i,1); // elimina un elemento a partire dalla posizione i
			card.print("*** REMOVE card:"+i);
			result = i; // restituisce la posizione della carta tolta.
			break;
		}
 	}
	return result; // carta non presente
};

// recupera la posizione all'interno dell'array.
Array.prototype.index = function(card) {
	var result = 0;
	var item;
	for (let i = 0; i < this.length; i++) {
 	   item = this[i];
		if (item.equals(card)){
			result = i; // restituisce l'indice
			break;
		}
 	}
	card.print("*** INDEX card:"+result);
	return result;
};

// inserisce/sposta una carta alla posizione indicata.
Array.prototype.insertAtPosition = function(index,card) {
	card.print("*** insertAtPosition card index:"+index);
	var previousPosition = this.index(card);
	card.print("*** insertAtPosition card previousPosition:"+previousPosition);
	var position_card_to_remove = this.remove(card);
	if(position_card_to_remove >= 0 && index > previousPosition){
		this.splice(index-1,0,card); // elimina un elemento a partire dalla posizione i
	}
	if(position_card_to_remove >= 0 && index < previousPosition){
		this.splice(index,0,card); // elimina 0 elementi a partire dalla posizione index
	}
	if(position_card_to_remove < 0 || index == previousPosition){
		this.splice(index,0,card); // elimina 0 elementi a partire dalla posizione index
	}
};

Array.prototype.listGameObjects = function() {
	var lResult = new Array();
	var card;
	for (let i = 0; i < this.length; i++) {
 	   card = this[i];
		lResult.push(card.gameObject);
 	}
	return lResult;
};


// ===================
// definizione partita
// ===================
class Game {
	constructor(id_game) {
		this.id_game = id_game;
		this.id_next_player = 1;
		this.players = [];
		this.pozzo = [];
		this.cards = [];	// carte totali necessarie alla partita
		this._makeDeck();
		this._shuffle(10);
	}

 	addPlayer(player){
    	this.players.push(player)
 	}

   	_shuffle(interazioni){
   		this.cards.shuffle(interazioni); // numero di interazioni
   	}

 	_makeDeck(){
		var mazzo1 = new Deck(1,"R"); 	// RED
		var mazzo2 = new Deck(2,"B");	// BLUE
		// this.cards = (mazzo1.add(mazzo2)).cards;
		this.add(mazzo1);
		this.add(mazzo2);
 	}

	add(deck){
		this.cards = this.cards.concat(deck.cards);
		return this;
	}

	getDescription(){
    	return `GAME #${this.id_game}`;
	}
};

// =====================
// definizione giocatore
// =====================
class Player {
	constructor(nome,cognome,alias,id,posizione) {
    	this.nome 		= nome;
    	this.cognome 	= cognome;
    	this.alias 		= alias;
    	this.id 		= id;
    	this.posizione 	= posizione;	// NORD, SUD, EST, OVEST
 	}

	getDescription(){
    	return `${this.nome} ${this.cognome} ${this.id}`;
	}
};

// ==========================
// definizione mazzo di carte
// ==========================
class Deck {
	constructor(id_mazzo, colore) {
    	this.id_mazzo  = id_mazzo;
		this.colore		= colore;
    	this.initialize();
 	}

	initialize(){
    	// console.log('initialize DECK ',this.id_mazzo);
		this.cards = new Array(54);	// sono 13 carte per 4 semi + 2 Jolly per ogni mazzo
		var index = 0;
		for (var seme in CONFIG.CARD_CONST.CARD_SEED){
			for (var nome in CONFIG.CARD_CONST.CARD_INDEX_SYMBOL){
				this.cards[index] = new Card(this.id_mazzo,CONFIG.CARD_CONST.CARD_INDEX_SYMBOL[nome],seme);
				index++;
			}
		}
		this.cards[52] = new Card(this.id_mazzo,"@","R"); // aggiungo il primo Jolly RED
		this.cards[53] = new Card(this.id_mazzo,"@","B"); // aggiungo il secondo Jolly BLUE
	}
};

// definizione singola carta
class Card {
	constructor(deck, symbol, seed) {
     	this.deck 	= deck;	// id mazzo di appartenenza
    	this.symbol = symbol;
		this.cSeed  = seed;
		this.value  = this.getValue();
    	this.joker  = "";  	// carta sostituita dal Joker quando fa parte di un gioco valido
    	this.id     = this.getId();
		this.target = "D"; // default is Deck
		this.front_back = "B"; // FRONT-BACK side
		this.position ={
						x :0,
						y :0
					};
		if(CONFIG.DEBUG){
			console.log("CARD is ",this.getId());
		}
	}

   // id_carta -> "AP1" asso picche del mazzo1
   getId(){
      return `${this.symbol}${this.cSeed}${this.deck}`;
   }

   getValue(){
	   return CONFIG.CARD_CONST.CARD_SYMBOL_VALUE[this.symbol];
   }
};

module.exports = {
 	Game 	: Game,
 	Deck  	: Deck,
 	Player 	: Player,
 	Card  	: Card
};
