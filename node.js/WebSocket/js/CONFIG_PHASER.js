var SCREEN_WIDTH  = 1024;
var SCREEN_HEIGHT = 600;

var GAME_CONFIG = {
   type  : Phaser.AUTO,
   parent: 'scala40Desk',     // DOM da sostituire
   width : SCREEN_WIDTH,
   height: SCREEN_HEIGHT,
   transparent : true,
   // backgroundColor:0xAABBCC,
   physics: {
      default: 'arcade',
      arcade: {
         debug: false
      }
   },
   callbacks:{
      postBoot:function test(){
			console.log("post boot");
		}
	}
};


export const ACTION_WAIT 	= "ACTION_WAIT";
export const ACTION_PESCA 	= "ACTION_PESCA";
export const ACTION_SCARTA = "ACTION_SCARTA";
export const PLAYER_ACTIONS = [ACTION_WAIT,ACTION_PESCA,ACTION_SCARTA]; // lista delle azioni che il PLAYER deve compiere.

export const DEBUG  = true;
export const STEP_DISPOSIZIONE_MAZZO_CARTE      = 0.1;   // di quanto vengono spostate le carte nel MAZZO di carte
export const INTERAZIONI_MESCOLARE_MAZZO_CARTE  = 20;    // interazioni per mescolare le carte.
export const GIOCA_CARTE_SCOPERTE               = false; // gioca con carte scoperte
export const MUOVI_CARTE_AVVERSARI              = false; // posso muovere le carte degli avversari?
export const WIDTH_CARD                         = 150;   // larghezza carta
export const HEIGHT_CARD                        = 192;   // altezza carta
export const NUMERO_CARTE_DA_DISTRIBUIRE        = 13;    // 13

export const SCALE_CARD_PLAYERS  = 0.7;
export const POSIZIONE_PLAYER_N  = {x:SCREEN_WIDTH/2,y:90};
export const POSIZIONE_PLAYER_E  = {x:950,y:300};
export const POSIZIONE_PLAYER_S  = {x:SCREEN_WIDTH/2,y:SCREEN_HEIGHT - 120};
export const POSIZIONE_PLAYER_W  = {x:60 ,y:300};
export const POSIZIONE_MAZZO_CARTE  = {x:950,y:250};
export const POSIZIONE_POZZO_CARTE  = {x:POSIZIONE_MAZZO_CARTE.x - WIDTH_CARD,y:POSIZIONE_MAZZO_CARTE.y};
export const DRAG_POZZO_ZONE        = {x:POSIZIONE_POZZO_CARTE.x,y:POSIZIONE_POZZO_CARTE.y,w:WIDTH_CARD*SCALE_CARD_PLAYERS+10,h:HEIGHT_CARD*SCALE_CARD_PLAYERS+10};
export const DRAG_PLAYER_SUD_ZONE   = {x:SCREEN_WIDTH/2 ,y:SCREEN_HEIGHT-HEIGHT_CARD/2,w:SCREEN_WIDTH-200,h:200};
export const CARD_STATE  = {SCARTATA:"Scartata",GIOCO:"Gioco"};


export var TARGET_PATH  = {
                     "N":POSIZIONE_PLAYER_N,   // lineTo(512, 90);  // North
							"E":POSIZIONE_PLAYER_E,   // lineTo(950, 300); // East
							"S":POSIZIONE_PLAYER_S,   // lineTo(512, 500); // South
							"W":POSIZIONE_PLAYER_W,   // lineTo(60, 300);  // West
                     "P":POSIZIONE_POZZO_CARTE,// lineTo(800, 300); // POZZO
                     "M":POSIZIONE_MAZZO_CARTE,// lineTo(800, 300); // MAZZO
						};
export var PLAYER_POS_ANGLE_ROTATION = {"N":180,"E":90,"S":0,"W":90,"P":180,"M":0};
// export var PLAYER_POS_X = {"N":550,"E":1050,"S": 550,"W":-20,"P":POSIZIONE_POZZO_CARTE.x,"M":POSIZIONE_MAZZO_CARTE.x};
// export var PLAYER_POS_Y = {"N":-50,"E":300,"S": 550,"W":300,"P":POSIZIONE_POZZO_CARTE.y,"M":POSIZIONE_MAZZO_CARTE.y};

// export const PLAYER_POSITION  = ["N","E","S","W"];
export const CARD_CONST = {
	CARD_SYMBOL : {		// indice->valore
		0:"A",
		1:"2",
		2:"3",
		3:"4",
		4:"5",
		5:"6",
		6:"7",
		7:"8",
		8:"9",
		9:"X",
		10:"J",
		11:"Q",
		12:"K",
		13:"@"
	},
	CARD_COLOR : {
		"RED":"R",
		"BLU":"B"
	},
	CARD_SEED : {
		"C":"cuori",
      "F":"fiori",
		"P":"picche",
      "Q":"quadri"
	},
	CARD_VALUE : {		// symbol->valore
		"A":1,
		"2":2,
		"3":3,
		"4":4,
		"5":5,
		"6":6,
		"7":7,
		"8":8,
		"9":9,
		"X":10,
		"J":10,
		"Q":10,
		"K":10,
		"@":25		// Jolly - il valore é variabile
	}
};

export const AUTOMA_CONST = {
	BASE_STRING_SEED : "QCPFQCP",
	INDEX_SEED_POSITION : {"Q":0,"C":1,"P":2,"F":3}, // array semi -> posizione
	INDEX_POSITION_SEED : {0:"Q",1:"C",2:"P",3:"F"}, // array posizione -> semi
	INDEX_SYMBOL_POSITION : {"A":0,
									 "2":1,
									 "3":2,
									 "4":3,
									 "5":4,
									 "6":5,
									 "7":6,
									 "8":7,
									 "9":8,
									 "X":9,
									 "J":10,
									 "Q":11,
									 "K":12,
									 "A":13 // serve per la seconda posizione dell'ASSO
								 },
	INDEX_POSITION_SYMBOL : {0 :"A",
									 1 :"2",
									 2 :"3",
									 3 :"4",
									 4 :"5",
									 5 :"6",
									 6 :"7",
									 7 :"8",
									 8 :"9",
									 9 :"X",
									 10:"J",
									 11:"Q",
									 12:"K",
									 13:"A" // serve per la seconda posizione dell'ASSO
									}
};
