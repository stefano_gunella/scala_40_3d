// ====================================
// Definizione Oggetti gestione Partita
// ====================================
var ACTION_NEW_GAME = {
   "action":"new_game",
   "game"  : null // Game
};

var ACTION_DRAG_CARD = {
   "action" :"drag",
   "card_id": null, // Card.id
   "position":{
       "x":0,
       "y":0
   }
};

module.exports = {
    ACTION_NEW_GAME	: ACTION_NEW_GAME,
    ACTION_DRAG_CARD: ACTION_DRAG_CARD
} ;
