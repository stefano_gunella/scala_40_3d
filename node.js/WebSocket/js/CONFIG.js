const DEBUG = true;
const PORT = 3000;
const ENCODING = 'utf-8';
const CARD_CONST = {
	CARD_INDEX_SYMBOL : {		// indice->valore
		1:"A",
		2:"2",
		3:"3",
		4:"4",
		5:"5",
		6:"6",
		7:"7",
		8:"8",
		9:"9",
		10:"X",
		11:"J",
		12:"Q",
		13:"K"
		//14:"@"  Jolly aggiunto a parte
	},
	CARD_COLOR : {
		"RED":"R",
		"BLU":"B"
	},
	CARD_SEED : {
		"Q":"quadri",
		"C":"cuori",
		"P":"picche",
      	"F":"fiori"
	},
	CARD_SYMBOL_VALUE : {		// symbol->valore
		"A":1,
		"2":2,
		"3":3,
		"4":4,
		"5":5,
		"6":6,
		"7":7,
		"8":8,
		"9":9,
		"X":10,
		"J":10,
		"Q":10,
		"K":10,
		"@":25		// Jolly - il valore é variabile
	}
};

const AUTOMA_CONST = {
	BASE_STRING_SEED : "QCPFQCP",
	INDEX_SEED_POSITION : {"Q":0,"C":1,"P":2,"F":3}, // array semi -> posizione
	INDEX_POSITION_SEED : {0:"Q",1:"C",2:"P",3:"F"}, // array posizione -> semi
	INDEX_SYMBOL_POSITION : {"A":0,
							 "2":1,
							 "3":2,
							 "4":3,
							 "5":4,
							 "6":5,
							 "7":6,
							 "8":7,
							 "9":8,
							 "X":9,
							 "J":10,
							 "Q":11,
							 "K":12,
							 "A":13 // serve per la seconda posizione dell'ASSO
						 },
	INDEX_POSITION_SYMBOL : {0 :"A",
							 1 :"2",
							 2 :"3",
							 3 :"4",
							 4 :"5",
							 5 :"6",
							 6 :"7",
							 7 :"8",
							 8 :"9",
							 9 :"X",
							 10:"J",
							 11:"Q",
							 12:"K",
							 13:"A" // serve per la seconda posizione dell'ASSO
							}
};

module.exports = {
	DEBUG		:DEBUG,
	PORT		:PORT,
 	DEBUG       :DEBUG,
 	CARD_CONST  :CARD_CONST,
 	AUTOMA_CONST:AUTOMA_CONST
};
