#!/usr/bin/env node
var CONFIG  = require('./js/CONFIG');
var DATA    = require('./js/DATA_STRUCTURE');
var WebSocketServer = require('websocket').server;
var http = require('http');

// list of users
var LIST_CLIENTS=[];

if (CONFIG.DEBUG) {
   console.log('*********** DEBUG *************');
   var card = new DATA.Card(1,"A","P");
   card.position.x = 120;
   card.position.y = 150;
   console.log(card);
   console.log('*******************************');
}

var server = http.createServer(function(request, response) {
    console.log((new Date()) + ' Received request for ' + request.url);
    response.writeHead(404);
    response.end();
});

server.listen(CONFIG.PORT, function() {
    console.log((new Date()) + ' Server is listening on port '+CONFIG.PORT);
});

wsServer = new WebSocketServer({
    httpServer: server,
    autoAcceptConnections: false
});

function originIsAllowed(origin) {
  // put logic here to detect whether the specified origin is allowed.
  return true;
}

wsServer.on('request', function(request) {
    console.log('...try to connect:',request);
    LIST_CLIENTS.push(request);
    if (!originIsAllowed(request)) {
      // Make sure we only accept requests from an allowed origin
      request.reject();
      console.log((new Date()) + ' Connection from origin ' + request.origin + ' rejected.');
      return;
    }
    console.log('...CONNECTED!');
    // var connection = request.accept('echo-protocol', request.origin);
    var connection = request.accept(null, request.origin);
    console.log((new Date()) + ' Connection accepted.');
    connection.on('message', function(message) {
        /* Questo non viene ricevuto
        if (message.type === 'utf8') {
            console.log('Received Message: ' + message.utf8Data);
            connection.sendUTF(message.utf8Data);
        }
        */
        if (message.type === "binary") {
            // var buf = Buffer.from(message.binaryData,"binary").toString("ascii");
            // console.log('Received Binary Message ' + message.binaryData + ' of ' + message.binaryData.length + ' bytes');
            console.log('Received Binary Message ' + message.binaryData);
            var data_string = Buffer.from(message.binaryData).toString("utf8");
            console.log('DATA STRING ' + data_string);
            // =========================
            // riconosce il tipo di JSON
            // =========================
            try{
                var data_json = JSON.parse(data_string);
                manage_JSON(connection, data_json);
            }
            catch(err){
                // console.log('*** NON E JSON ***'+data_string.position);
            }
        }
    });
    connection.on('close', function(reasonCode, description) {
        console.log((new Date()) + ' Peer ' + connection.remoteAddress + ' disconnected.');
    });
});

function sendMessagePlayers(message, ws, id) {
    console.log('sendMessagePlayers : ', id);
    if (LIST_CLIENTS[id] !== ws) {
        console.log('IF : ', message);
        for (var i = 0; i < CLIENTS.length; i++) {
            CLIENTS[i].send(message);
        }
    }else{
        console.log('ELSE : ', message);
    }
}

// ==========================================================
// Qui avviene la gestione dei comandi provenienti dal CLIENT
// ==========================================================
function manage_JSON(connection, data_json){
    console.log("data_json TYPE is " + data_json.type );
    switch (data_json.type) {
        case "Action":
            // Mescola il mazzo
            if(data_json.action == "shake"){
                console.log("*** Shake Deck ***",data_json);
                // qui deve mescolare il mazzo e restituire una carta
                // guarda la class Partita
                var card = new DATA.Card(1,"A","P");
                card.position.x = 120;
                card.position.y = 150;
                console.log("FIRST CARD IS " + JSON.stringify(card));
                let json_2_string = JSON.stringify(card);
                let buffer = Buffer.from(json_2_string,CONFIG.ENCODING);
                connection.sendBytes(buffer);
            }
            if(data_json.action == "drag"){
                // drag card
                console.log("*** drag card ***",data_json.card);
                var card = new DATA.Card(data_json.card.deck,data_json.card.name,data_json.card.cSeed);
                DATA.ACTION_DRAG_CARD.card = card;
                let json_2_string = JSON.stringify(DATA.ACTION_DRAG_CARD);
                let buffer = Buffer.from(json_2_string,CONFIG.ENCODING);
                // TODO: rimettere ---> connection.sendBytes(buffer);
                connection.sendBytes(buffer);
            }
            break;
        case "Card":

            break;
        default:

    }
    // var myMessage = Buffer.alloc(8,"STOCAZZO", "utf8");


}
