#!/usr/bin/env node
// Da utilizzare con la controparte cliente index.html

var WebSocketServer = require('websocket').server;
var http = require('http');
const fs = require('fs');
const WebSocket = require('ws');
var PORT = 3000;

var server = http.createServer(function(request, response) {
    // Qui possiamo processare la richiesta HTTP
    // Dal momento che ci interessano solo le WebSocket, non dobbiamo implementare nulla
	// console.log("request.method :",request.method);
	console.log("request.client._readableState.defaultEncoding:",request.client._readableState.defaultEncoding);
	response.writeHead(200, { 'content-type': 'text/html' });
    fs.createReadStream('index.html').pipe(response);
}).listen(PORT, function() {
 	console.log("START SERVER ON PORT:",PORT);
});
// Creazione del server
wsServer = new WebSocketServer({
    httpServer: server
});
// Gestione degli eventi
wsServer.on('request', function(request) {
	// console.log("REQUEST :",request);
    // var connection = request.accept(null, request.origin);
	var connection = request.accept('echo-protocol', request.origin);
    connection.on('message', function(message) {
        // Metodo eseguito alla ricezione di un messaggio
        if (message.type === 'utf8') {
            // Se il messaggio è una stringa, possiamo leggerlo come segue:
            console.log('Il messaggio ricevuto è: ' + message.utf8Data);
			connection.sendUTF(message.utf8Data);
        }
    });
    connection.on('close', function(connection) {
        // Metodo eseguito alla chiusura della connessione
    });
});
