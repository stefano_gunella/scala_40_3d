=============================
Add these packages to Node.js
=============================
$ npm install ws
$ npm install --save-optional bufferutil		<--- Allows to efficiently perform operations such as masking and unmasking the data payload of the WebSocket frames.
$ npm install --save-optional utf-8-validate	<--- Allows to efficiently check if a message contains valid UTF-8.
$ npm install express --save

See /doc/ws.md for Node.js-like documentation of ws classes and utility functions.

Creazione applicazione EXPRESS di base
$ npm install express-generator -g
