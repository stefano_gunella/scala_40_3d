#!/usr/bin/env node
// var express = require("express");
const WebSocket = require('ws');
var wss = new WebSocket.Server({port:3000});
var CONFIG   = require('./js/CONFIG');
var DATA     = require('./js/DATA_STRUCTURE');
var JSON_CMD = require('./js/JSON_COMMANDS');

// list of users
var LIST_CLIENTS={};
var index = 0;

if (CONFIG.DEBUG) {
   console.log('======== DEBUG ========');
   var card = new DATA.Card(1,"A","P");
   card.position.x = 120;
   card.position.y = 150;
   console.log(card);
   console.log('======== NEW GAME ========');
   var game = newGame();
   console.log(game);
   console.log('=======================');
}

// check if connection is established
wss.on('connection', function(ws) {
    console.log('connection is established!!!',ws);
    LIST_CLIENTS[index] = ws;
    var ws_index = index;
    index++;

    console.log('******** ADD connection to dictionary');
    ws.on('message', function(message) {
        console.log('received: %s', message);   // message is a string...
        var message_json = JSON.parse(message); // message_json is a JSON...
        // ======
        // ACTION
        // ======
        console.log('ACTION is %s',message_json.action);
        switch (message_json.action) {
            // =====
            // LOGIN
            // =====
            case "login":
                onsole.log('LOGIN-TODO');
                break;
            case "new_game":
                // ========
                // NEW GAME
                // ========
                var game = newGame();
                sendGameToPlayers(game);
                break;
            case "drag":
                // ======
                // DRAG
                // ======
                dragCard(ws_index,message_json);
                break;
            default:
                console.log("*** default ***");
        }
    });
});

function newGame() {
    console.log("*** newGame ***");
    // qui deve mescolare il mazzo e restituire una carta
    // guarda la class Partita
    var partita = new DATA.Game("GameTEST"); // codice fittizio
    // QUI DOVREBBE RECUPERARE I GIOCATORI DAL DB...
    // TODO - GET PLAYERS
    var pl1 = new DATA.Player("PL1","TEST","PL1","ID1","N")  // player 1 NORD
    var pl2 = new DATA.Player("PL2","TEST","PL2","ID2","S")  // player 1 SUD
    partita.addPlayer(pl1);
    partita.addPlayer(pl2);
    /*
    var card = new DATA.Card(1,"A","P");
    card.position.x = 120;
    card.position.y = 150;
    console.log("FIRST CARD IS " + JSON.stringify(card));
    */
    return partita;
}

function sendGameToPlayers(game) {
    JSON_CMD.ACTION_NEW_GAME.game = game;
    let json_2_string = JSON.stringify(JSON_CMD.ACTION_NEW_GAME);
    _sendAll(json_2_string);
}

function dragCard(ws_index,json) {
    JSON_CMD.ACTION_DRAG_CARD.card_id = json.card_id;
    JSON_CMD.ACTION_DRAG_CARD.position.x = json.position.x;
    JSON_CMD.ACTION_DRAG_CARD.position.y = json.position.y;
    let json_2_string = JSON.stringify(JSON_CMD.ACTION_DRAG_CARD);
    _sendAllExceptSender(json_2_string, ws_index);
}

function _sendAllExceptSender(message, ws_index) {
    console.log('sendAllExceptSender : ', ws_index);
    for (const [key,ws] of Object.entries(LIST_CLIENTS)) {
        if (key !== ws_index) {
            ws.send(message);
        }
    }
}

function _sendAll(message) {
    console.log('_sendAll...');
    for (const [key,ws] of Object.entries(LIST_CLIENTS)) {
        //console.log('MESSAGE : ', key);
        //console.log('VALUE : ', ws);
        ws.send(message);
    }
}
