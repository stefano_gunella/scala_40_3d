class_name Card extends Area2D

# ============================
# SEGNALI GENERATI DALLE CARTE
# ============================
signal card_discarted
signal card_taken
# ============================

var is_pressed = false
var is_dragging = false
var is_on_placeholder_S = false
var relative_position:Vector2 = Vector2(0,0)
var img_card_atlas = load("res://assets/mazzoFranceseNEW.png")
var card_atlas_texture_front = AtlasTexture.new()
var card_atlas_texture_back	 = AtlasTexture.new()

var deck:int	# mazzo di appartenenza
var symbol 		# simpbolo della carta [A23456789XJQK*]
var cSeed 		# QCPF
var value:int		# punti di valore per arrivare a 40
var joker		# contiene la carta sostituita dal Joker
var id 			# id univoco carta = nome+seme+mazzo
var target		# D=Deck,P=Pozzo,P[N|S|E|W]=Player Nord|Sud|Est|West
var front_back	# show FRONT or BACK side
var _json		# define JSON 

func _ready():
	print("ecm --> ",ROOT.ecm)
	self.connect("card_discarted", Callable(ROOT.ecm, "_on_card_discarted").bind(self))
	self.connect("card_taken", Callable(ROOT.ecm, "_on_card_taken").bind(self))

# =====================================================
# Costruttore a partire dal JSON proveniente dal SERVER
# =====================================================
func init_by_json(card_json):
	deck		= card_json.deck
	symbol 		= card_json.symbol
	cSeed 		= card_json.cSeed
	value		= card_json.value
	joker		= card_json.joker
	id 			= card_json.id
	front_back	= card_json.front_back
	target		= card_json.target
	position.x 	= card_json.position.x
	position.y 	= card_json.position.y
	print("init_by_json --> ",deck)
	_json = {
			"deck"		: deck,
			"symbol"	: symbol,
			"cSeed"		: cSeed,
			"value"		: value,
			"joker"		: joker,
			"id"		: id,
			"front_back": front_back,
			"target"	: target,
			"position":{
				"x":0,
				"y":0
				}
			}
	_makeTextureFront()
	_makeTextureBack()
	set_texture()

# ==================================================================================
# Genera la texture per la carta in base al proprio ID : AQ1 <--- Asso,Quadri,Mazzo1
# ==================================================================================
func _makeTextureFront():
	print("TEXTURE FRONT ",deck)
	card_atlas_texture_front.atlas = img_card_atlas
	# =============================
	# qui recupera l'area del Jolly
	# =============================
	if (symbol == "@"):	
		var pox_x = CONFIG.DECK_JSON.deck[symbol+cSeed].x
		var pos_y = CONFIG.DECK_JSON.deck[symbol+cSeed].y
		card_atlas_texture_front.region = Rect2(pox_x,
												pos_y,
												CONFIG.DECK_JSON.deck.w,
												CONFIG.DECK_JSON.deck.h)
	else:
		# qui recupera l'area di tutte le altre carte
		var index_x = CONFIG.ORDER_DISP_NAMES[symbol] - 1
		card_atlas_texture_front.region = Rect2(index_x * (CONFIG.DECK_JSON.deck.w + CONFIG.DECK_JSON.deck.margin.h),
												CONFIG.DECK_JSON.deck[cSeed].y,
												CONFIG.DECK_JSON.deck.w,
												CONFIG.DECK_JSON.deck.h)
	card_atlas_texture_front.filter_clip = true
	set_scale(Vector2(0.5,0.5))

# ======================
# Genera la texture BACK
# ======================
func _makeTextureBack():
	print("TEXTURE BACK ", deck)
	card_atlas_texture_back.atlas = img_card_atlas
	var black_red = "B" if (deck == 1) else "R" # Black or Red Back
	var pox_x = CONFIG.DECK_JSON.deck["B"+black_red].x
	var pos_y = CONFIG.DECK_JSON.deck["B"+black_red].y
	card_atlas_texture_back.region = Rect2(	pox_x,
											pos_y,
											CONFIG.DECK_JSON.deck.w,
											CONFIG.DECK_JSON.deck.h)
	card_atlas_texture_back.filter_clip = true
	set_scale(Vector2(0.5,0.5))
	
# ==============================================
# Restituisce la stringa che rappresenta il JSON
# ==============================================
func json_2_string():
	var result = JSON.stringify(self.JSON.new().stringify(),"\t")
	print(">>>>>>> result :",result)
	return result

# ==============================================
# Restituisce la stringa che rappresenta il JSON
# ==============================================
func json_WS():
	var result = JSON.stringify(self.JSON.new().stringify()).to_utf8_buffer() 
	print(">>>>>>> result :",result)
	return result

# ==============================================
# Restituisce la stringa che rappresenta il JSON
# ==============================================
func JSON.new().stringify():
	_json.id = symbol + cSeed + String(deck)
	_json.position.x = position.x
	_json.position.y = position.y
	return _json

# =========================
# Abilita TOUCH della carta 
# =========================
func enable_touch():
	input_pickable = true
	
func disable_touch():
	input_pickable = false	

func set_texture():
	set_front_texture() if(front_back == "F") else set_back_texture()

func set_front_texture():
	print("set_front_texture")
	$CardSprite.texture = card_atlas_texture_front
	front_back = "F"

func set_back_texture():
	print("set_back_texture")
	$CardSprite.texture = card_atlas_texture_back
	front_back = "B"

# warning-ignore:unused_argument
func _process(delta):
	if(is_dragging):
		#print(">>>>>>> drag ",get_viewport().get_mouse_position())
		set_position(get_viewport().get_mouse_position())

# warning-ignore:unused_argument
# warning-ignore:unused_argument
func _on_Card_input_event(viewport, event, shape_idx):
	if event is InputEventMouseButton:
		if event.is_pressed():
			# CARTA SELEZIONATA
			is_pressed = true
			ROOT.selected_card = self
		else:
			# CARTA RILASCIATA
			is_pressed = false
			ROOT.ecm._card_dragged(self)
			if (is_on_placeholder_S):
				print(">>>>>>> CARTA PRESA ",is_on_placeholder_S)
				emit_signal("card_taken")
	if event is InputEventMouseMotion:
		if (is_pressed and event.get_velocity().length() > 0):
			# CARTA TRASCINATA
			is_dragging = true
		else:
			is_dragging = false
