class_name GameRoot extends Node2D

func _ready():
	ROOT.gameRoot = self
	var wsm = WebSocketManager.new()
	add_child(wsm)
	ROOT.wsm = wsm
	if CONFIG.DEBUG:
		ROOT.game.current_player.posizione = "S"
		
		$Card3C.id = "TEST-3C" 
		$Card3C.deck = 1 
		$Card3C.symbol = "3" 
		$Card3C.cSeed = "C" 
		$Card3C.visible = true
		$Card3C.front_back = "F"
		$Card3C._makeTextureFront()
		$Card3C._makeTextureBack()
		$Card3C.set_texture()
		$Card3C.enable_touch()
		$buttons_debug.visible = true

# ========
# New Game
# ========
func newGame(game_json):
	var game = Game.new(game_json) #new game!
	add_child(game)
	ROOT.game = game

func dragCard(card_json):
	print("Find card to drag:",card_json.card_id)
	var card = ROOT.game.get_card_by_id(card_json.card_id)
	# deve recuperare la carta e muoverla
	if(card != null):
		card.position = Vector2(card_json.position.x,card_json.position.y)

func add_card_to_deck(card:Card):
	card.position = $Decks.position
	card.add_to_group("g_decks")
	add_child(card)

func add_card_to_player(player:Player,card:Card):
	player.l_cards.push_front(card)	
