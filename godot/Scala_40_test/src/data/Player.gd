class_name Player extends PanelContainer

var id
var nome
var cognome
var alias
var posizione
var _json
var l_cards

# ===================
# definizione partita
# ===================
func _init(player_json): 
	print("Player FROM JSON...",player_json)
	id 		= player_json.id
	nome 	= player_json.nome
	cognome = player_json.cognome
	alias 	= player_json.alias
	posizione = player_json.posizione	# NORD, SUD, EST, OVEST

	_json = {
			"id"		: id,
			"nome"		: nome,
			"cognome"	: cognome,
			"alias"		: alias,
			"posizione"	: posizione
			}
