extends Node

var DEBUG = true
var SERVER_ADDRESS = "192.168.178.22" 
# var SERVER_ADDRESS = "127.0.0.1" 
var SERVER_PORT = "3000" 
var ORDER_DISP_NAMES = {"A":1,"2":2,"3":3,"4":4,"5":5,"6":6,"7":7,"8":8,"9":9,"X":10,"J":11,"Q":12,"K":13,"@":14}
var ORDER_DISP_SEEDS = {"Q":1,"C":2,"P":3,"F":4}
var ORDER_JACK = {"J1":1,"J2":2}
var DECK_JSON = null
const CARD_SCENE = preload("res://src/scene/Card.tscn")
const MENU_DEBUG = preload("res://src/scene/MenuDebug.tscn")

func _ready():
	var loader_json_file = File.new()
	var err = loader_json_file.open("res://assets/mazzoFranceseNEW.json", File.READ)
	var test_json_conv = JSON.new()
	test_json_conv.parse(loader_json_file.get_as_text()).result
	DECK_JSON = test_json_conv.get_data()
	loader_json_file.close()
