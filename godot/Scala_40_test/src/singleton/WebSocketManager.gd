# =================================
# controparte SERVER : ws_server.js - class_name WebSocketManager 
# =================================
class_name WebSocketManager extends Node

# The URL we will connect to
var websocket_url = "ws://{SERVER_ADDRESS}:{SERVER_PORT}".format({"SERVER_ADDRESS":CONFIG.SERVER_ADDRESS,"SERVER_PORT":CONFIG.SERVER_PORT})

# Our WebSocketClient instance
var _client = WebSocketClient.new()

func _ready():
	print(">>> WEB SOCKET IS READY <<<")
	# Connect base signals to get notified of connection open, close, and errors.
	_client.connect("connection_closed", Callable(self, "_connection_closed"))
	_client.connect("connection_error", Callable(self, "_connection_error"))
	_client.connect("connection_established", Callable(self, "_connection_established"))
	# This signal is emitted when not using the Multiplayer API every time
	# a full packet is received.
	# Alternatively, you could check get_peer(1).get_available_packets() in a loop.
	_client.connect("data_received", Callable(self, "_data_received"))

	# Initiate connection to the given URL.
	var conn = _client.connect_to_url(websocket_url)
	if conn != OK:
		print("Unable to connect")
		set_process(false)
	else:
		print("connect OK")

func _connection_closed(was_clean = false):
	# was_clean will tell you if the disconnection was correctly notified
	# by the remote peer before closing the socket.
	print("Closed, clean: ", was_clean)
	set_process(false)

func _connection_error():
	print("Connection error")
	print(_client.get_peer(1))

func _connection_established(proto):
	# This is called on connection, "proto" will be the selected WebSocket
	# sub-protocol (which is optional)
	print("******** connected *********")
	# You MUST always use get_peer(1).put_packet to send data to server,
	# and not put_packet directly when not using the MultiplayerAPI.
	_client.get_peer(1).put_packet(JSON.stringify(CLIENT_SERVER_COMMANDS.NEW_GAME_COMMAND,"\t").to_utf8_buffer())

func _data_received():
	var data = _client.get_peer(1).get_packet().get_string_from_utf8()
	print("Got data from server...")
	if data != null:
		var test_json_conv = JSON.new()
		test_json_conv.parse(data).result # da string a JSON
		var data_json = test_json_conv.get_data()
		#if data_json.error == OK
		print("data_json type : ",data_json.action)
		# =======================
		# Qui si decide cosa fare
		# =======================
		print("do some action ",data_json.action)
		match data_json.action:
			"drag":
				print(">>>>>>>>>>>> SERVER drag card ")
				ROOT.gameRoot.dragCard(data_json)
			"new_game":
				print(">>>>>>>>>>>> SERVER NEW GAME ")
				ROOT.gameRoot.newGame(data_json.game)
			"discard_card":
				print("discard_card ",data_json)
			"get_card_from_deck":
				print("get_card_from_deck ",data_json)

func _process(delta):
	delta = 1
	# Call this in _process or _physics_process. Data transfer, and signals
	# emission will only happen when calling this function.
	if _client.get_connection_status() == _client.CONNECTION_CONNECTING || _client.get_connection_status() == _client.CONNECTION_CONNECTED:
		_client.poll()
	#if _client.get_peer(1).is_connected_to_host():
	#	print("is_connected_to_host!!!")

func drag_card(card_json):
	# _client.get_peer(1).put_var(card.to_json())
	CLIENT_SERVER_COMMANDS.DRAG_CARD.card_id = card_json.id
	CLIENT_SERVER_COMMANDS.DRAG_CARD.position.x = card_json.position.x
	CLIENT_SERVER_COMMANDS.DRAG_CARD.position.y = card_json.position.y
	# var DRAG_CARD = {"type":"action","action":"drag","card":card.to_json()}
	_client.get_peer(1).put_packet(JSON.stringify(CLIENT_SERVER_COMMANDS.DRAG_CARD,"\t").to_utf8_buffer() )
