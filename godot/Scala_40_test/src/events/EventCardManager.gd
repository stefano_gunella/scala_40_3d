class_name EventCardManager extends Node

func _ready():
	ROOT.ecm = self

func _card_dragged(card:Card):
	print(">>>>>> CLIENT DRAG Card :",card.id,"-",card.target) 
	ROOT.wsm.drag_card(card) # comunica la posizione della carta al server
	#rpc_config() <--- da verificare

func _on_card_discarted(card:Card):
	print("--- EVENT _on_card_discarted ",card.id)

# ======================================
# Intercetta l'evento di una carta presa
# ======================================
func _on_card_taken(card:Card):
	print("AAAAAAAAAAAAAA _on_card_taken ",card.id)
	_on_flip_card(card)
	ROOT.game.remove_last_card_from_deck()
	ROOT.game.add_card_to_player(ROOT.game. card)
	ROOT.game.enable_last_card_touch_from_deck()
	
func _on_flip_card(card:Card):
	if(card != null and card.front_back == "F"):
		print("--- flip_card_front_back ",card.id)
		ROOT.cap.root_node = card.get_path()
		ROOT.cap.play("flip_card_front_back")
	else:
		print("--- flip_card_back_front ",card.id)
		ROOT.cap.root_node = card.get_path()
		ROOT.cap.play("flip_card_back_front")

