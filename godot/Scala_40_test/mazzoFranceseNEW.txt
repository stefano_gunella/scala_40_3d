deck {
	width: 152,
	height: 192,
	margin {
		h: 4,
		v: 2
	},
	pos_quadri {
		x: 0,
		y: 582
	},
	pos_cuori {
		x: 0,
		y: 0
	},
	pos_picche {
		x: 0,
		y: 388
	},
	pos_fiori {
		x: 0,
		y: 194
	},
	pos_jolly_1 {
		x: 770,
		y: 776
	},
	pos_jolly_2 {
		x: 1078,
		y: 776
	},
	pos_front {
		x: 923,
		y: 0
	},
	pos_back {
		x: 1232,
		y: 0
	}
}
