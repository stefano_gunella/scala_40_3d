class_name Game extends Node # refactoring --> GameManager

var id_game			# id della partita corrente
var id_next_player	# id turno prossimo giocatore
var l_players = []	# n. di giocatori presenti nella partita
var l_pozzo = []	# pozzo
var l_cards = []	# carte mescolate
var h_all_cards = {}# riferimento a tutte le carte mescolate
var _json			# define JSON 
var game_json = null
var current_player:Player # giocaore di turno

# ===================
# definizione partita
# ===================
func _init(_game_json): 
	id_game			= _game_json.id_game
	id_next_player 	= _game_json.id_next_player
	l_pozzo 		= _game_json.pozzo
	self.game_json	= _game_json
	print("PLAYERS...",l_players.size())
	
func _ready():
	for pl in game_json.players:
		print(">>> player ",pl.nome)
		_add_player(Player.new(pl))
	# ===============================
	# Crea il mazzo di carte dal JSON
	# ===============================
	for card_json in game_json.cards:
		var card = CONFIG.CARD_SCENE.instance()
		card.init_by_json(card_json)
		_add_card_to_deck(card)
	enable_last_card_touch_from_deck()
	_json = {
			"id_game"		: id_game,
			"id_next_player": id_next_player,
			"players"		: l_players,
			"pozzo"			: l_pozzo,
			"cards"			: l_cards
			}

func _add_player(player):
	print("_add_player ",player.id)
	l_players.push_front(player)
	var placeholder = get_node('../placeHolderPlayer' + player.posizione)
	placeholder.add_child(player)

func _add_card_to_deck(card:Card):
	print("_add_card_to_deck ",card.id) # DEBUG
	l_cards.push_front(card)
	ROOT.gameRoot.add_card_to_deck(card)
	h_all_cards[card.id] = card

func remove_last_card_from_deck():
	l_cards.pop_front()

func enable_last_card_touch_from_deck():
	var card = l_cards.front()
	if(card != null):
		card.enable_touch()

func add_card_to_player(player:Player, card:Card):
	print("add_card_to_player ",card.id) # DEBUG
	l_cards.push_front(card)
	ROOT.gameRoot.add_card_to_player(player,card)
	
func get_card_by_id(id):
	print("_get_card_by_id ",id)
	if (id.length()!=3):
		return null
	return h_all_cards[id]
	
