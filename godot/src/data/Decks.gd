# ======================================================================
# Contiene i mazzi mescolati nella configurazione proveniente dal server
# ======================================================================
class_name Decks extends Sprite

var list_cards = [] # array contenente le carte

var _json = {
			"list_cards" : list_cards,
			"position":{
				"x":0,
				"y":0
				}
			}

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# ==============================================
# Restituisce la stringa che rappresenta il JSON
# ==============================================
func json_2_string():
	var result = JSON.print(self.to_json(),"\t").to_utf8() 
	return result

# ===========================================================
# Restituisce la stringa che rappresenta il JSON dell'oggetto
# ===========================================================
func to_json():
	_json.position.x = position.x
	_json.position.y = position.y
	return _json
